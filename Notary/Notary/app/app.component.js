"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var AppComponent = /** @class */ (function () {
    function AppComponent() {
    }
    AppComponent.prototype.ShowEditModal = function () {
        $('#EditModal').modal('show');
    };
    AppComponent.prototype.ShowMessagesModal = function () {
        $('#MessagesModal').modal('show');
    };
    AppComponent.prototype.ShowNotesModal = function () {
        $('#NotesModal').modal('show');
    };
    AppComponent.prototype.ShowHistoryModal = function () {
        $('#HistoryModal').modal('show');
    };
    AppComponent.prototype.ShowEnvoyerModal = function () {
        $('#EnvoyerModal').modal('show');
    };
    AppComponent.prototype.ShowDocumentModal = function () {
        $('#DocumentsModal').modal('show');
    };
    AppComponent.prototype.ShowAnnoncesModal = function () {
        $('#AnnoncesModal').modal('show');
    };
    AppComponent.prototype.CloseEditModal = function () {
        $('#EditModal').modal('hide');
    };
    AppComponent.prototype.CloseMessagesModal = function () {
        $('#MessagesModal').modal('hide');
    };
    AppComponent.prototype.CloseNotesModal = function () {
        $('#NotesModal').modal('hide');
    };
    AppComponent.prototype.CloseHistoryModal = function () {
        $('#HistoryModal').modal('hide');
    };
    AppComponent.prototype.CloseEnvoyerModal = function () {
        $('#EnvoyerModal').modal('hide');
    };
    AppComponent.prototype.CloseDocumentModal = function () {
        $('#DocumentsModal').modal('hide');
    };
    AppComponent.prototype.CloseAnnoncesModal = function () {
        $('#AnnoncesModal').modal('hide');
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            templateUrl: './app.component.termplate.html'
        })
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map