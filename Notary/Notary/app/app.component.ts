import { Component, Input } from '@angular/core';


declare var $: any;

@Component({
    selector: 'my-app',
    templateUrl: './app.component.termplate.html' 
})
export class AppComponent  {

    ShowEditModal() {
        $('#EditModal').modal('show');
    }

    ShowMessagesModal() {
        $('#MessagesModal').modal('show');
    }

    ShowNotesModal() {
        $('#NotesModal').modal('show');
    }

    ShowHistoryModal() {
        $('#HistoryModal').modal('show');
    }

    ShowEnvoyerModal() {
        $('#EnvoyerModal').modal('show');
    }

    ShowDocumentModal() {
        $('#DocumentsModal').modal('show');
    }

    ShowAnnoncesModal() {
        $('#AnnoncesModal').modal('show');
    }

    CloseEditModal() {
        $('#EditModal').modal('hide');
    }

    CloseMessagesModal() {
        $('#MessagesModal').modal('hide');
    }

    CloseNotesModal() {
        $('#NotesModal').modal('hide');
    }

    CloseHistoryModal() {
        $('#HistoryModal').modal('hide');
    }

    CloseEnvoyerModal() {
        $('#EnvoyerModal').modal('hide');
    }

    CloseDocumentModal() {
        $('#DocumentsModal').modal('hide');
    }

    CloseAnnoncesModal() {
        $('#AnnoncesModal').modal('hide');
    }
}
