﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Notary.Startup))]
namespace Notary
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
